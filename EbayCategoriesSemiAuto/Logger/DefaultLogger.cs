﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCategoriesSemiAuto.Logger
{
    public class DefaultLogger : ILogger
    {
        private List<string> logs = new List<string>();
        private string logfile = "log" + DateTime.Now.Ticks + ".txt";

        public void Log(string log)
        {
            logs.Add(log);
            File.WriteAllLines(logfile, logs);
        }

        public void Dispose()
        {
            File.WriteAllLines(logfile, logs);
        }
    }
}
