﻿using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft;
using Newtonsoft.Json;
using System.Xml;
namespace EbayCategories
{
    public class SupplierCollection
    {
        private static MongoDatabase db = null;
        private static MongoCollection<BsonDocument> collection = null;
        private static string eanpath = "";

        private static MongoDatabase GetDatabase(string dbname)
        {
            if (db == null)
            {
                string constring = String.Format("mongodb://client148:client148devnetworks@148.251.0.235/{0}", dbname);

                db = new MongoClient(constring)
                    .GetServer()
                    .GetDatabase(dbname);
            }

            return db;
        }

        public static MongoCollection<BsonDocument> GetCollection(string dbname)
        {
            if (collection == null)
            {
                string colname = dbname.Replace("PD_", "").ToLower().Replace("berk", "berks").Replace("bremer", "bremeritems").Replace("knv", "knvbooks");

                collection = GetDatabase(dbname)
                    .GetCollection<BsonDocument>(colname);
            }

            return collection;
        }

        public static List<string> GetEans(string dbname, string eanpath2)
        {
            var NoEbayCategories = GetCollection(dbname).Find(Query.And(Query.NotExists("plus.ebaycategory"))).SetFlags(QueryFlags.NoCursorTimeout).SetFields(eanpath2);

            List<string> eans = new List<string>();

            foreach (var noEbayCat in NoEbayCategories)
            {
                noEbayCat.Remove("_id");
                string json = noEbayCat.ToJson();
                XmlDocument doc = JsonConvert.DeserializeXmlNode(json);

                string[] split = eanpath2.Split('.');
                string eanfield = split[split.Count() - 1];
                var eanNode = doc.SelectSingleNode("//" + eanfield);

                if (eanNode != null)
                {
                    eans.Add(eanNode.InnerText);
                }

                #region oldcode
                //string[] split = eanfield.Split('.');

                //string ean = "";

                //BsonDocument bson = null;
                
                //int counter = 0;
                //foreach (string s in split)
                //{
                //    counter++;
                //    if (bson == null)
                //    {
                //        bson = noEbayCat[s].AsBsonDocument;
                //    }
                //    else if (counter < split.Count())
                //    {
                //        bson = bson[s].AsBsonDocument;
                //    }
                //}

                //if (bson.Contains(split[split.Count() - 1]))
                //{
                //    ean = bson[split[split.Count() - 1]].AsString;
                //}

                //if (ean != "")
                //{
                //    eans.Add(ean);
                //}
                #endregion
            }

            eanpath = eanpath2;

            return eans;
        }

        public static void UpdateEbayCategory(string ean)
        {
            collection.Update(Query.EQ(eanpath, ean), Update.Set("plus.ebaycategory", eanpath));
        }

        
    }
}
