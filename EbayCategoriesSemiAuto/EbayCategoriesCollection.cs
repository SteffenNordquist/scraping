﻿using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCategories
{
    public class EbayCategoriesCollection
    {
        private static MongoDatabase db = null;
        private static MongoCollection<BsonDocument> collection = null;

        private static MongoDatabase GetDatabase()
        {
            if (db == null)
            {
                string constring = String.Format("mongodb://client148:client148devnetworks@148.251.0.235/{0}", "EbayCategories");

                db = new MongoClient(constring)
                    .GetServer()
                    .GetDatabase("EbayCategories");
            }

            return db;
        }

        private static MongoCollection<BsonDocument> GetCollection()
        {
            if (collection == null)
            {
                collection = GetDatabase()
                    .GetCollection<BsonDocument>("ebaycategories");
            }

            return collection;
        }

        public static void Insert(List<EbayCategoryEntityTemp> ebaycategories)
        {
            foreach (EbayCategoryEntityTemp ebaycat in ebaycategories)
            {
                SupplierCollection.UpdateEbayCategory(ebaycat.eans[0]);

                var testCatExistence = GetCollection().Find(Query.EQ("category", ebaycat.category));
                if (testCatExistence.Count() == 0)
                {
                    GetCollection().Insert(ebaycat);
                    
                }
                else
                {
                    var testExistence = GetCollection().Find(Query.EQ("eans", ebaycat.eans[0]));
                    if (testExistence.Count() == 0)
                    {
                        GetCollection().Update(Query.EQ("category", ebaycat.category), Update<EbayCategoryEntityTemp>.Push(x => x.eans, ebaycat.eans[0]));
                        
                    }
                }
            }
        }
    }
}
