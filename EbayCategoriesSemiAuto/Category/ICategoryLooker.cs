﻿using EbayCategoriesSemiAuto.Ebay;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCategoriesSemiAuto.Category
{
    public interface ICategoryLooker
    {
        IEnumerable<CategoryEntity> GetCategories(string id);
    }
}
