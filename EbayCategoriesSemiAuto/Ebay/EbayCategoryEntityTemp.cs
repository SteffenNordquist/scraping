﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCategories
{
    public class EbayCategoryEntityTemp
    {
        public List<string> eans { get; set; }
        public string category { get; set; }
        public BsonDocument plus { get; set; }
    }
}
