﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace EbayCategories
{
    public class EbayWebsite
    {
        private List<string> eans = new List<string>();
        private WebClient webclient = null;
        private List<EbayCategoryEntityTemp> ebaycategories = new List<EbayCategoryEntityTemp>();

        public EbayWebsite(List<string> eans)
        {
            this.eans = eans;
            this.webclient = new WebClient();
            this.webclient.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");
           
        }

        public void ProcessEans()
        {
            
            foreach (string ean in eans)
            {
                List<string> eanss = new List<string>();
                eanss.Add(ean);

                ebaycategories.Add(new EbayCategoryEntityTemp { eans = eanss, category = GetCategory(ean) });
            }

        }

        public List<EbayCategoryEntityTemp> GetEbayCategories()
        {
            return ebaycategories.Select(x => x).Where(x => x.category != "").ToList();
        }

        private string GetCategory(string ean)
        {
            string ebaysearchLink = String.Format("http://www.ebay.de/sch/i.html?_from=R40&_sacat=0&_nkw={0}&_rdc=1", ean);

            string pagesource = GetPageSource(ebaysearchLink);

            //string pagesource = webclient.DownloadString(ebaysearchLink);

            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(pagesource);

            var listingNodes = doc.DocumentNode.SelectNodes("//li[@class='sresult lvresult clearfix li']");

            if (listingNodes != null && listingNodes.Count() > 0)
            {
                var listingNode = listingNodes.First();
                string a = listingNode.SelectSingleNode(listingNode.XPath + "//a").Attributes["href"].Value.ToString();

                //pagesource = webclient.DownloadString(a);
                pagesource = GetPageSource(a);
                doc.LoadHtml(pagesource);

                var lastCatNode = doc.DocumentNode.SelectNodes("//li[@class='bc-w']").Last();
                string itemlink = lastCatNode.SelectSingleNode(lastCatNode.XPath + "/a").Attributes["href"].Value.ToString();

                string[] split = itemlink.Split('/');
                string ebaycategory = split[split.Count() - 2];

                return ebaycategory;
            }

            return string.Empty;
        }

        private string GetPageSource(string link)
        {
            //WebRequest request = HttpWebRequest.Create(link);
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(link);
            request.Timeout = 20 * 60 * 1000;
            request.KeepAlive = false;
            request.ProtocolVersion = HttpVersion.Version10; 
            //var response = r.GetResponse();
            
            string pagesource = "";
            //StreamReader readStream = new StreamReader(response.GetResponseStream(), Encoding.UTF8);
            using (WebResponse response = request.GetResponse())
            {
                using (Stream stream = response.GetResponseStream())
                {
                    using (StreamReader readStream = new StreamReader(stream, Encoding.UTF8))
                    {
                        pagesource = readStream.ReadToEnd();
                    }
                }
            }

            //response.Dispose();
            //readStream.Dispose();

            return pagesource;
        }
    }
}
