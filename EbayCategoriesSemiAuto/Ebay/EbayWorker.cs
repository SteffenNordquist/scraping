﻿using EbayCategoriesSemiAuto.Category;
using EbayCategoriesSemiAuto.CategoryPersistent;
using EbayCategoriesSemiAuto.Logger;
using EbayCategoriesSemiAuto.Suppliers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCategoriesSemiAuto.Ebay
{
    public class EbayWorker : IEbayWorker
    {
        private readonly ICategoryLooker categoryLooker;
        private readonly ICategoryPersist categoryPersist;
        private readonly ISupplierCategoryIndicator supplierCategoryIndicator;
        private readonly ILogger logger;

        public EbayWorker(ICategoryLooker categoryLooker, ICategoryPersist categoryPersist, ISupplierCategoryIndicator supplierCategoryIndicator, ILogger logger)
        {
            this.categoryLooker = categoryLooker;
            this.categoryPersist = categoryPersist;
            this.supplierCategoryIndicator = supplierCategoryIndicator;
            this.logger = logger;
        }

        public void ProcessIds(IEnumerable<string> ids, string idFieldPath, bool encloseIdWithQuotes)
        {
            using (logger)
            {
                foreach (string id in ids)
                {
                    string searchId = id;

                    if (encloseIdWithQuotes)
                    {
                        searchId = "\"" + searchId + "\"";
                    }

                    List<CategoryEntity> categories = (List<CategoryEntity>)categoryLooker.GetCategories(searchId);

                    if (categories != null && categories.Count() > 0)
                    {
                        if (categories.Last().categoryNumber != null)
                        {
                            List<string> eans = new List<string>();
                            eans.Add(id.Replace("\"",""));

                            categoryPersist.Save(new CategoryPersistenceEntity
                            {
                                specificcategory = categories.Last().categoryNumber.ToString(),
                                eans = eans,
                                categories = categories
                            });

                            supplierCategoryIndicator.Update(idFieldPath, id.Replace("\"",""));

                            string log = id + " => " + string.Join("|", categories.Select(x=>x.categoryNumber + ":" + x.categoryText));

                            logger.Log(log);
                        }
                    }
                }
            }
        }
    }
}
