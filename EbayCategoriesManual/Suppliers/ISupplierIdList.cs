﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCategoriesManual.Suppliers
{
    public interface ISupplierIdList
    {
        IEnumerable<string> GetIdList(string idPath, string dbName);
    }
}
