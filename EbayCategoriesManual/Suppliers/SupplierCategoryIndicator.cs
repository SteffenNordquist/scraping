﻿using DN.DataAccess;
using DN.DataAccess.ConnectionFactory;
using EbayCategoriesManual.SupplierDBConstructor;
using EbayCategoriesManual.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCategoriesManual.Suppliers
{
    public class SupplierCategoryIndicator : ISupplierCategoryIndicator
    {
        private IDbConnection dbConnection;

        public SupplierCategoryIndicator(IDBConstructor dbConstructor, ISettings settings)
        {
            this.dbConnection = dbConstructor.Create<string>(settings.GetValue("dbname"));
        }

        public void Update(string idPath, string id)
        {
            string query = "{ \"" + idPath + "\" : \"" + id + "\"}";
            dbConnection.DataAccess.Commands.Update<string>(query, "plus.ebaycategory", idPath);
        }

    }
}
