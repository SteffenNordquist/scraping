﻿
using DN.DataAccess.ConnectionFactory;
using EbayCategoriesManual.SupplierDBConstructor;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace EbayCategoriesManual.Suppliers
{
    public class BKSupplierIdList : ISupplierIdList
    {
        private readonly IDBConstructor dbConstructor;

        public BKSupplierIdList(IDBConstructor dbConstructor)
        {
            this.dbConstructor = dbConstructor;
        }

        public IEnumerable<string> GetIdList(string idPath, string dbName)
        {
            IDbConnection dbConnection = dbConstructor.Create<string>(dbName);

            string query = "{ \"plus.ebaycategory\" : {$exists : false} }";
            string[] setFields = new string[1];
            setFields[0] = idPath;
            var docs = dbConnection.DataAccess.Queries.Find<BsonDocument>(query, setFields);
            
            List<string> idList = new List<string>();

            foreach (var doc in docs)
            {
                //remove _id so json serialization wont fail
                doc.Remove("_id");

                string json = doc.ToJson();

                XmlDocument xmlDoc = JsonConvert.DeserializeXmlNode(json);

                string[] split = idPath.Split('.');

                string eanfield = split[split.Count() - 1];
                var eanNode = xmlDoc.SelectSingleNode("//" + eanfield);

                if (eanNode != null)
                {
                    idList.Add(eanNode.InnerText);
                }

            }

            return idList;

        }
    }
}
