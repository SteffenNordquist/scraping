﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCategoriesManual.Entities
{
    public class SearchData
    {
        public string searchString { get; set; }
        public string idField { get; set; }
    }
}
