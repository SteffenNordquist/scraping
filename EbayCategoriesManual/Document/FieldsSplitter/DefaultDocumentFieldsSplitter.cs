﻿using MongoDB.Bson;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace EbayCategoriesManual.Document.FieldsSplitter
{
    public class DefaultDocumentFieldsSplitter : IDocumentFieldsSplitter
    {
        private XmlDocument sampleXml = null;
        private List<string> fields = new List<string>();

        public List<string> GetFields<T>(T document)
        {
            BsonDocument castedDocument = (BsonDocument)(object)document;

            castedDocument.Remove("_id");
            castedDocument.Remove("plus");

            string json = castedDocument.ToJson();
            XmlDocument doc = JsonConvert.DeserializeXmlNode(json, "root");

            sampleXml = doc;

            foreach (XmlNode node in doc.ChildNodes[0].ChildNodes)
            {
                if (node.HasChildNodes)
                {
                    GetField(node, node.Name);
                }
                else
                {
                    string field = node.Name;
                    if (!fields.Contains(field))
                    {
                        fields.Add(field.Replace(".#text", ""));
                    }
                }
            }

            return fields;
        }

        private void GetField(XmlNode node, string parent)
        {

            foreach (XmlNode n in node.ChildNodes)
            {
                if (n.HasChildNodes)
                {
                    if (parent != "")
                    {
                        GetField(n, parent + "." + n.Name);
                    }
                    else
                    {
                        GetField(n, n.Name);
                    }
                }
                else
                {
                    if (parent != "")
                    {
                        if (!fields.Contains((parent + "." + n.Name).Replace(".#text", "")))
                        {
                            fields.Add((parent + "." + n.Name).Replace(".#text", ""));
                        }
                    }
                    else
                    {
                        if (!fields.Contains((parent + "." + n.Name).Replace(".#text", "")))
                        {
                            fields.Add(n.Name.Replace(".#text", ""));
                        }
                    }
                }
            }

        }
    }
}
