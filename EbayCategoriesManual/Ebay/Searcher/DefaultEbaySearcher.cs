﻿using EbayCategoriesManual.Ebay.Category;
using EbayCategoriesManual.Ebay.Html;
using EbayCategoriesManual.Entities;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCategoriesManual.Ebay.Searcher
{
    public class DefaultEbaySearcher : IEbaySearcher
    {
        private readonly IHtmlDownloader htmlDownloader;

        private readonly ICategoryLooker categoryLooker;

        public DefaultEbaySearcher(IHtmlDownloader htmlDownloader, ICategoryLooker categoryLooker)
        {
            this.htmlDownloader = htmlDownloader;
            this.categoryLooker = categoryLooker;
        }

        public IEnumerable<SearchResult> Search(string text)
        {
            List<SearchResult> searchresults = new List<SearchResult>();

            string ebaysearchLink = String.Format("http://www.ebay.de/sch/i.html?_from=R40&_sacat=0&_nkw={0}&_rdc=1", text);

            string pagesource = htmlDownloader.GetHtml(ebaysearchLink);


            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(pagesource);

            List<HtmlNode> listingNodes = new List<HtmlNode>();

            var resultsContainer = doc.DocumentNode.SelectSingleNode("//ul[@id='ListViewInner']");

            if (resultsContainer != null)
            {
                var li_elements = resultsContainer.Elements("li");

                if (li_elements != null && li_elements.Count() > 0)
                {
                    listingNodes = li_elements.Where(x => x.Attributes["class"].Value.Contains("sresult lvresult clearfix li")).Select(x => x).ToList();

                }
            }

            //var listingNodes = doc.DocumentNode.SelectNodes("//li[@class='sresult lvresult clearfix li']");

            if (listingNodes != null && listingNodes.Count() > 0)
            {
                foreach (var listingNode in listingNodes.Take(10))
                {
                    string itemlink = listingNode.SelectSingleNode(listingNode.XPath + "//a").Attributes["href"].Value.ToString();
                    string imagelink = listingNode.SelectSingleNode(listingNode.XPath + "//img").Attributes["src"].Value.ToString();
                    string title = listingNode.SelectSingleNode(listingNode.XPath + "//h3[@class='lvtitle']").InnerText.Trim();
                    string price = listingNode.SelectSingleNode(listingNode.XPath + "//li[@class='lvprice prc']").InnerText.Trim();

                    List<CategoryEntity> categories = categoryLooker.GetCategories(itemlink).ToList();

                    searchresults.Add(new SearchResult
                    {
                        itemlink = itemlink,
                        imagelink = imagelink,
                        title = title,
                        price = price,
                        categories = categories
                    });


                }
            }

            return searchresults;
        }
    }
}
