﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCategoriesManual.Ebay.Category.Parser
{
    public interface ICategoryTextParser
    {
        string GetCategoryText(HtmlNode sourceNode);
    }
}
