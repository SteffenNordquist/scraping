﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace EbayCategoriesManual.Ebay.Html
{
    public class HtmlDownloader : IHtmlDownloader
    {
        public string GetHtml(string link)
        {
            string pagesource = "";

            try
            {
                //WebRequest request = HttpWebRequest.Create(link);
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(link);
                request.Timeout = 20 * 60 * 1000;
                request.KeepAlive = false;
                request.ProtocolVersion = HttpVersion.Version10;
                //var response = r.GetResponse();


                //StreamReader readStream = new StreamReader(response.GetResponseStream(), Encoding.UTF8);
                using (WebResponse response = request.GetResponse())
                {
                    using (Stream stream = response.GetResponseStream())
                    {
                        using (StreamReader readStream = new StreamReader(stream, Encoding.UTF8))
                        {
                            pagesource = readStream.ReadToEnd();
                        }
                    }
                }
            }
            catch { }

            //response.Dispose();
            //readStream.Dispose();

            return pagesource;
        }
    }
}
