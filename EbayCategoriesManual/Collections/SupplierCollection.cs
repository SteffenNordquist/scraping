﻿using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft;
using Newtonsoft.Json;
using System.Xml;
using EbayCategoriesManual.Entities;
namespace EbayCategoriesManual
{
    public class SupplierCollection
    {
        private MongoDatabase db = null;
        private MongoCollection<BsonDocument> collection = null;

        private MongoDatabase GetDatabase(string dbname)
        {
            if (db == null)
            {
                string constring = String.Format("mongodb://client148:client148devnetworks@136.243.44.111/{0}", dbname);

                db = new MongoClient(constring)
                    .GetServer()
                    .GetDatabase(dbname);
            }

            return db;
        }

        public MongoCollection<BsonDocument> GetCollection(string dbname)
        {
            if (collection == null)
            {
                string colname = dbname.Replace("PD_", "").ToLower().Replace("berk", "berks").Replace("bremer", "bremeritems").Replace("knv", "knvbooks");

                collection = GetDatabase(dbname)
                    .GetCollection<BsonDocument>(colname);
            }

            return collection;
        }

        public List<string> GetEans(string dbname, string eanpath)
        {
            var NoEbayCategories = GetCollection(dbname).Find(Query.And(Query.NotExists("plus.ebaycategory"))).SetFlags(QueryFlags.NoCursorTimeout).SetFields(eanpath);

            List<string> eans = new List<string>();

            foreach (var noEbayCat in NoEbayCategories)
            {
                noEbayCat.Remove("_id");
                string json = noEbayCat.ToJson();
                XmlDocument doc = JsonConvert.DeserializeXmlNode(json);

                string[] split = eanpath.Split('.');
                string eanfield = split[split.Count() - 1];
                var eanNode = doc.SelectSingleNode("//" + eanfield);

                if (eanNode != null)
                {
                    eans.Add(eanNode.InnerText);
                }

                #region oldcode
                //string[] split = eanfield.Split('.');

                //string ean = "";

                //BsonDocument bson = null;
                
                //int counter = 0;
                //foreach (string s in split)
                //{
                //    counter++;
                //    if (bson == null)
                //    {
                //        bson = noEbayCat[s].AsBsonDocument;
                //    }
                //    else if (counter < split.Count())
                //    {
                //        bson = bson[s].AsBsonDocument;
                //    }
                //}

                //if (bson.Contains(split[split.Count() - 1]))
                //{
                //    ean = bson[split[split.Count() - 1]].AsString;
                //}

                //if (ean != "")
                //{
                //    eans.Add(ean);
                //}
                #endregion
            }

            return eans;
        }

        //private XmlDocument sampleXml = null;
        //private List<string> fields = new List<string>();
        //public List<string> GetFields(string dbname)
        //{
        //    var item = GetCollection(dbname).FindOne();
        //    item.Remove("_id");

        //    string json = item.ToJson();
        //    XmlDocument doc = JsonConvert.DeserializeXmlNode(json, "root");

        //    sampleXml = doc;

        //    foreach (XmlNode node in doc.ChildNodes[0].ChildNodes)
        //    {
        //        if (node.HasChildNodes)
        //        {
        //            GetField(node, node.Name);
        //        }
        //        else
        //        {
        //            string field = node.Name;
        //            fields.Add(field.Replace(".#text", ""));
        //        }
        //    }

        //    return fields;
        //}

        //public XmlDocument GetSampleXml()
        //{
        //    return this.sampleXml;
        //}

        //private void GetField(XmlNode node, string parent)
        //{

        //    foreach (XmlNode n in node.ChildNodes)
        //    {
        //        if (n.HasChildNodes)
        //        {
        //            if (parent != "")
        //            {
        //                GetField(n, parent + "." + n.Name);
        //            }
        //            else
        //            {
        //                GetField(n, n.Name);
        //            }
        //        }
        //        else
        //        {
        //            if (parent != "")
        //            {
        //                fields.Add((parent + "." + n.Name).Replace(".#text", ""));
        //            }
        //            else
        //            {
        //                fields.Add(n.Name.Replace(".#text", ""));
        //            }
        //        }
        //    }

        //}

        //public Dictionary<string, SearchData> GetCollectionItems(string idField, string searchField)
        //{
        //    return GetCollection("").Find(Query.And(Query.NotExists("plus.ebaycategory"))).SetFields(idField, searchField).SetFlags(QueryFlags.NoCursorTimeout).ToDictionary(x => GetElement(x, idField), x => new SearchData { searchString = GetElement(x, searchField), idField = idField });
            
            
        //}
        //private string GetElement(BsonDocument bson, string field)
        //{
        //    BsonValue val = null;
        //    string[] split = field.Split('.');

        //    int counter = 1;
        //    foreach (string f in split)
        //    {
        //        if (counter == split.Count())
        //        {
        //            val = bson.GetElement(f).Value;
        //        }
        //        else
        //        {
        //            bson = bson.GetElement(f).Value.ToBsonDocument();
        //        }
        //        counter++;
        //    }

        //    return val.ToString();
        //}

        public void UpdateEbayCategory(string id, string idpath)
        {
            GetCollection("").Update(Query.EQ(idpath, id), Update.Set("plus.ebaycategory", idpath));
        }



        
    }
}
