﻿using DN.DataAccess;
using DN.DataAccess.ConnectionFactory;
using DN.DataAccess.ConnectionFactory.Mongo;
using EbayCategoriesManual.SupplierDBConstructor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCategoriesManual.CategoryPersistent
{
    public class CategoryPersist : ICategoryPersist
    {
        private readonly IDbConnection dbConnection;

        public CategoryPersist()
        {
            #region dn data access
            IDbConnectionFactory connectionFactory = new MongoConnectionFactory(new MongoConnectionProperties());
            connectionFactory.ConnectionProperties.SetProperty("connectionstring", String.Format("mongodb://client148:client148devnetworks@136.243.44.111/{0}", "EbayCategories"));
            connectionFactory.ConnectionProperties.SetProperty("databasename", "EbayCategories");
            connectionFactory.ConnectionProperties.SetProperty("collectionname", "ebaycategoriespaths");
            #endregion

            dbConnection = connectionFactory.CreateConnection();
        }

        public void Save(CategoryPersistenceEntity category)
        {

            var testExistence = dbConnection.DataAccess.Queries.FindByKey<CategoryPersistenceEntity>("_id", category.specificcategory);
            if (testExistence == null)
            {
                dbConnection.DataAccess.Commands.Insert<CategoryPersistenceEntity>(category);
            }
            else
            {
                var eanExistence = dbConnection.DataAccess.Queries.FindByKey<CategoryPersistenceEntity>("eans", category.eans.First());
                if (eanExistence == null)
                {
                    string updatequery = "{ _id : \"" + category.specificcategory + "\" }";
                    dbConnection.DataAccess.Commands.AddToSet<string>(updatequery, "eans", category.eans.First());
                }
            }
        }
    }
}
